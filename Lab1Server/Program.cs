﻿using System;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Lab1Client
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {

                Console.Write("# ");
                string str = Console.ReadLine();
                Socket socket = null;
                try
                {
                    socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    socket.Connect("localhost", 11111);
                    socket.ReceiveBufferSize = 1024;
                    byte[] bytes = Encoding.ASCII.GetBytes(str);
                    socket.Send(bytes);

                    str = "";
                    bytes = new byte[1024];
                    int data = socket.Receive(bytes);
                    str += Encoding.ASCII.GetString(bytes, 0, data);
                    Console.WriteLine(str);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                finally
                {
                    if (socket != null)
                        socket.Close();
                }
   
            }
        }
    }
}
